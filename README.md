This is DataTree structure of data. ROOT (v5 or v6) is needed to build this library.
__Neither QA, cuts nor macro nor any logic are expected in current package.__


## INSTALLATION

```
cd < DataTree directory >/
```

Make build directory:

```
mkdir build
cd build
```

Initialize your ROOT environment with `source <path-to-root>/bin/thisroot.sh`. Make sure, your compiler is the same as it was for root.

To check it on lxplus, say

```
$ which root
/afs/cern.ch/sw/lcg/app/releases/ROOT/5.34.19/i686-slc6-gcc47-opt/root/bin/root
```

*i686-slc6-gcc47-opt* is the needed one. It won't work for the case of default root, but in this case it is assumed it was built with default environment so the next step is not neccessary.

To use specific compiler, do

```
source source /afs/cern.ch/sw/lcg/contrib/gcc/4.7/i686-slc6-gcc47-opt/setup.sh
export CC=/afs/cern.ch/sw/lcg/contrib/gcc/4.7/i686-slc6-gcc47-opt/bin/gcc
export CXX=/afs/cern.ch/sw/lcg/contrib/gcc/4.7/i686-slc6-gcc47-opt/bin/g++
``` 

Prepare build

```
cmake [-DCMAKE_INSTALL_PREFIX=\<desired location\>] [-DEXPERIMENT=<SHINE or CBM>] ../src/
```

Set -DCMAKE_INSTALL_PREFIX=\<desired location\> if you want to install this library
and don't want this library to be installed globally (i.e. /usr/).

Build library via make:

```
make
```

If you want to install it, call

```
make install
```

Shared library libDataTree.so is ready to use.

## USAGE:

### Within ROOT

Open ROOT:

```
root -l
```
Load shared library:
```
root [0]  gSystem->Load("libDataTree.so")
(int)0
root [1]  // do whatever you want
```


### Inside other C++ project

To add DataTree into your CMake project, add the following lines into your CMakeLists.txt:

```
list(APPEND CMAKE_PREFIX_PATH $ENV{DATATREE_HOME})

...

find_package(DataTree REQUIRED)
```