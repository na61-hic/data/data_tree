## General statements

DataTree is a package consisting of data entities: DataTreeEvent, DataTreeTrack, etc

Neither logic nor QA are expected in this package.

DataTree __MUST__ be compatible with ROOT 5 and 6. At least with v5.34/36 which is default for ShineOffline on lxplus. 
In particular that means...


## CI

Continuous integration now is served by native GitLab CI. Recently only build step is implemented but there are plans to add automatic deployment and compatibility tests also. To add specific root version into a list of build/test/deploy cycle, please, help yourself and add lines into .gitlab-ci.yml in the root of project or ask me or Viktor to do that. 



## Known pitfalls

### stl containter as a datamember

Be careful with std::array, std::map and other uncommon containers as they are not fully supported by *cint*, especially in ROOT 5.

Typical signature of this case is the following error during dictionary generation


>Error: string() declared but not defined prec_stl/vector:497:Error: vector<string,allocator<string> >() declared but not defined prec_stl/vector:497:

>Warning: Error occurred during reading source files

>Warning: Error occurred during dictionary source generation

__even #include<> statement with incompatible container may cause this error__

So the solution is to remove all presence of uncommon stl containers from DataTree code. 

### ROOT\_GENERATE\_DICTIONARY

Plenty of errors could raise on this step. ROOT\_GENERATE\_DICTIONARY is a buggy CMake function with signature changing from release to release. Before you start doing something with *CMakeLists.txt*, please, checkout `rootcint -h` first or make an issue and assign it to me and I'll do it.


