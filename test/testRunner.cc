//
// Created by eugene on 8/16/18.
//

#include "testDataTreeBasic.hh"
//#include "testDataTreeCompatibility.hh"

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);

  RUN_ALL_TESTS();

  return 0;
}