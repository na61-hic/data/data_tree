//
// Created by eugene on 7/27/18.
//

#ifndef DATATREE_TESTDATATREECOMPATIBILITY_HH
#define DATATREE_TESTDATATREECOMPATIBILITY_HH

#include <TFile.h>
#include <TSystem.h>
#include <TSystemDirectory.h>
#include <gtest/gtest.h>

using namespace std;

namespace {

class DataTreeCompatibilityTest : public ::testing::Test, public ::testing::WithParamInterface<string> {

};

vector<string> GetDataTreeFiles() {
  auto *cwdNameBuf = new char[1000];
  getcwd(cwdNameBuf, 1000);
  TSystemDirectory cwd(cwdNameBuf, cwdNameBuf);

  TList *files = cwd.GetListOfFiles();
  vector<string> dataTreeFiles;
  dataTreeFiles.reserve(static_cast<unsigned long>(files->GetEntries()));

  if (files) {
    TSystemFile *file;
    TString fname;
    TIter next(files);
    while ((file = (TSystemFile *) next())) {
      fname = file->GetName();
      if (!file->IsDirectory() && fname.EndsWith(".root")) {
        dataTreeFiles.push_back(fname.Data());
      }
    }
  }

  return dataTreeFiles;
}

TEST_P(DataTreeCompatibilityTest, OpenDataTree) {
  const char *dataTreeFileName = GetParam().c_str();

  Long_t id;
  Long_t size;
  Long_t flags;
  Long_t modtime;
  int fStat = gSystem->GetPathInfo(dataTreeFileName, &id, &size, &flags, &modtime);

  ASSERT_EQ(fStat, 0);

  std::unique_ptr<TFile> dataTreeFilePtr(TFile::Open(dataTreeFileName));
  EXPECT_TRUE((*dataTreeFilePtr).IsOpen());
  (*dataTreeFilePtr).Close();
}

vector<string> dataTreeFiles = GetDataTreeFiles();

INSTANTIATE_TEST_CASE_P(
    someThing,
    DataTreeCompatibilityTest,
    ::testing::ValuesIn(dataTreeFiles.begin(), dataTreeFiles.end())
);

} //namespace

#endif



