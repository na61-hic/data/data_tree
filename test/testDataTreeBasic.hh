//
// Created by eugene on 8/16/18.
//

#ifndef DATATREE_TESTDATATREEBASIC_HH
#define DATATREE_TESTDATATREEBASIC_HH

#include <gtest/gtest.h>
#include <DataTreeEvent.h>

namespace {

    TEST(DataTreeBasic, DataTreeAddPSD) {
        DataTreeEvent ev;
        ASSERT_EQ(ev.GetNPSDModules(), 0);
        ASSERT_EQ(ev.GetNMCPSDModules(), 0);

        ev.AddPSDModule();
        ev.AddMCPSDModule();

        ASSERT_EQ(ev.GetNPSDModules(), 1);
        ASSERT_EQ(ev.GetNMCPSDModules(), 1);

    }

    TEST(DataTreeBasic, DataTreeAddPSDPrimaryParticle) {
        DataTreeEvent ev;

        ASSERT_EQ(ev.GetNPSDPrimaryParticles(), 0);
        auto *pp = ev.AddPSDPrimaryParticle();
        pp->SetA(207);
        pp->SetZ(82);
        ASSERT_EQ(ev.GetNPSDPrimaryParticles(), 1);
        pp = ev.AddPSDPrimaryParticle();
        pp->SetZ(79);
        pp->SetA(197);
        ASSERT_EQ(ev.GetNPSDPrimaryParticles(), 2);

        std::for_each(std::begin(*ev.GetPSDPrimaryParticles()), std::end(*ev.GetPSDPrimaryParticles()),
                      [](TObject *o) {
                          o->Print();
                          auto *pp1 = (DataTreePSDPrimaryParticle *) o;
                          std::cout << "Z = " << pp1->GetZ() << "; A = " << pp1->GetA() << std::endl;
                      });

        ev.ClearEvent();
        ASSERT_EQ(ev.GetNPSDPrimaryParticles(), 0);


    }

}

#endif //DATATREE_TESTDATATREEBASIC_HH
