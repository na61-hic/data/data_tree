
message ("<GitVersion.cmake>")

find_package(Git)

if (Git_FOUND)

    unset(GIT_DESCRIBE)
    execute_process(COMMAND ${GIT_EXECUTABLE} describe --tags
            OUTPUT_VARIABLE GIT_DESCRIBE
            WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
            ERROR_QUIET
            OUTPUT_STRIP_TRAILING_WHITESPACE
            )
    
    if (GIT_DESCRIBE)
        string (REGEX MATCH "^(.*)-(.*)-(.*)$" GIT_DESCRIBE_MATCH ${GIT_DESCRIBE})
    
        if (GIT_DESCRIBE_MATCH)
            set (GIT_RECENT_TAG ${CMAKE_MATCH_1})
            set (GIT_COMMITS_AHEAD ${CMAKE_MATCH_2})
            set (GIT_COMMIT ${CMAKE_MATCH_3})
        endif ()

    endif () # GIT_DESCRIBE

    message("revision: ${GIT_DESCRIBE}")
    message("tag: ${GIT_RECENT_TAG}")
    message("commit: ${GIT_COMMIT}")

endif () # Git_FOUND

message ("</GitVersion.cmake>")
