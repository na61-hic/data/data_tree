#ifndef DataTreePSDSection_H
#define DataTreePSDSection_H 1

#include <vector>
#include <iostream>
#include "TClonesArray.h"
#include "TObject.h"

class DataTreePSDSection : public TObject
{
    
public:
  
    DataTreePSDSection(int idx = 0);
    ~DataTreePSDSection();
    
    void ClearEvent() {fEnergy = 0.;}
    
    int GetId() const {return fId;}
    double GetEnergy() const {return fEnergy;}

    void SetEnergy(double value) {fEnergy = value;}
    void AddEnergy(double value) {fEnergy += value;}
    void SetId(int idx) {fId = idx;}
    
private:    
    
    int fId;
    double fEnergy;
    
    ClassDefNV(DataTreePSDSection, 1)
};

#endif
