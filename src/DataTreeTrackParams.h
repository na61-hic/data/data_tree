#ifndef DataTreeTrackParams_H
#define DataTreeTrackParams_H 1

#include <vector>
#include <TClonesArray.h>
#include <TObject.h>
#include <TLorentzVector.h>

class DataTreeTrackParams : public TObject
{
    
public:

    DataTreeTrackParams(int idx = 0);
    ~DataTreeTrackParams();
    
    void SetParameters(const std::vector <double> data) { fParams = data; }
    void SetCovMatrix(const std::vector <double> data) { fCovMatrix = data; }
    
    std::vector <double> GetParameters() const { return fParams; }
    std::vector <double> GetCovMatrix() const { return fCovMatrix; }
    
    void SetMomentum(const TLorentzVector value) { fMomentum = value; }
    TLorentzVector GetMomentum() const { return fMomentum; }
    
//     void SetZ(double z) { fZ = z;}
//     double GetZ() {return fZ;}
    
    void SetPosition(const float x, const float y, const float z) 
    { 
        fPosition[0] = x;
        fPosition[1] = y;
        fPosition[2] = z;
    }
    float GetX() const { return fPosition[0]; }
    float GetY() const { return fPosition[1]; }
    float GetZ() const { return fPosition[2]; }
    
    void SetMagFieldFit(float cx0, float cx1, float cx2,
                        float cy0, float cy1, float cy2,
                        float cz0, float cz1, float cz2,
                        float z0)
    {
        fMagFieldFit[0] = cx0;
        fMagFieldFit[1] = cx1;
        fMagFieldFit[2] = cx2;
        fMagFieldFit[3] = cy0;
        fMagFieldFit[4] = cy1;
        fMagFieldFit[5] = cy2;
        fMagFieldFit[6] = cz0;
        fMagFieldFit[7] = cz1;
        fMagFieldFit[8] = cz2;
        fMagFieldFit[9] = z0;        
    }
    
    const float* GetMagFieldFit() const { return fMagFieldFit; }
    
private:

    std::vector <double> fParams;
    std::vector <double> fCovMatrix;
    
    float fMagFieldFit[10];
    
    TLorentzVector fMomentum;
    
//     double fZ;
    float fPosition[3];
    
    
    ClassDef(DataTreeTrackParams, 2);
};

#endif
