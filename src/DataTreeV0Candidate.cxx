#include "DataTreeV0Candidate.h"

DataTreeV0Candidate::DataTreeV0Candidate(int idx) : TObject(),
    fDaughters (new TClonesArray("DataTreeV0Candidate")),
    fNumberOfDaughters(0)
{
    SetId(idx);
}
DataTreeV0Candidate::~DataTreeV0Candidate()
{
    
}

ClassImp(DataTreeV0Candidate)

