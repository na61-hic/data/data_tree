#ifndef DataTreeMCTrack_H
#define DataTreeMCTrack_H 1

#include <Rtypes.h>

#include <vector>
#include <TClonesArray.h>
#include <TObject.h>
#include <TLorentzVector.h>

#include "DataTreeConstants.h"
#include "DataTreeTrackMatch.h"

class DataTreeTrackMatch;

class DataTreeMCTrack : public TObject
{
    
public:
  
    DataTreeMCTrack(UInt_t idx = 0);
    ~DataTreeMCTrack();
    
    UInt_t GetId() const {return fId;}
    void SetUndefinedValues();
    
    double GetPt() const { return fMomentum.Pt(); }
    double GetPhi() const { return fMomentum.Phi(); }
    double GetEta() const { return fMomentum.Eta(); }
    double GetPx() const { return fMomentum.Px(); }
    double GetPy() const { return fMomentum.Py(); }
    double GetPz() const { return fMomentum.Pz(); }
    double GetP() const { return fMomentum.P(); }
    double GetRapidity() const { return fMomentum.Rapidity(); }
    double GetEnergy() const { return fMomentum.E(); }
    TLorentzVector GetMomentum() const { return fMomentum; }
    
    Int_t GetPdgId() const {return fPdgId;}
    Int_t GetCharge() const {return fCharge;}
    int GetMotherId() const {return fMotherId;}
    
    double GetTOFPositionComponent(int idx) const {return fTOFPosition[idx];}
    double GetPSDPositionComponent(int idx) const {return fPSDPosition[idx];}
    double GetPSDEnergy() const {return fPSDEnergy;}
    bool IsSpectator() const { return fIsSpectator; }


    int GetPSDSectionId() const {return fPSDSectionId;}
    int GetTOFSegmentId() const {return fTOFSegmentId;}
    int GetNumberOfHits(int subidx = 0) const { return fNumberOfHits[subidx]; }

    void SetId(UInt_t idx){fId = idx;}
    void SetPdgId(Int_t value){ fPdgId = value;}
    void SetCharge(Int_t value){ fCharge = value;}
    void SetMomentum(const TLorentzVector& value);

    void SetMotherId(int value){fMotherId = value;}

    void SetTOFPosition(double x, double y, double z){fTOFPosition[0]=x; fTOFPosition[1]=y; fTOFPosition[2]=z;}
    void SetTOFPositionComponent(int idx, double value){fTOFPosition[idx] = value;}
    
    void SetPSDPosition(double x, double y, double z){fPSDPosition[0]=x; fPSDPosition[1]=y; fPSDPosition[2]=z;}
    void SetPSDPositionComponent(int idx, double value){fPSDPosition[idx] = value;}

    void SetPSDEnergy(double value){fPSDEnergy = value;}

    void SetPSDSectionId(int idx){ fPSDSectionId = idx;}
    void SetTOFSegmentId(int idx){ fTOFSegmentId = idx;}
    void SetNumberOfHits(int value, int subidx = 0){ fNumberOfHits[subidx] = value; }
    void SetSpectator(bool is) { fIsSpectator = is; }
private:


    static const int nSubDetectors = 5;
    
    UInt_t fId;
    bool fIsSpectator;

    TLorentzVector fMomentum;
    
    Int_t fPdgId;		//pdg code
    Int_t fCharge;		//charge
    int fMotherId;		//mother id (-1 == primary)
    
    double fTOFPosition[3];	//Position at TOF
    double fPSDPosition[3];	//Position at PSD
    double fPSDEnergy;		//sum over PsdPoints for a given MC track

    int fNumberOfHits[nSubDetectors];
    
    int fPSDSectionId;		//id of the corresponding PSD section
    int fTOFSegmentId;		//id of the corresponding TOF segment
    

    ClassDefNV(DataTreeMCTrack, 3);

};

#endif
