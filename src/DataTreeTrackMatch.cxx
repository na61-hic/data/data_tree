/*
 * DataTreeTrackMatch.cpp
 *
 *  Created on: Mar 1, 2018
 *      Author: eugene
 */

#include "DataTreeTrackMatch.h"

ClassImp(DataTreeTrackMatch);

DataTreeTrackMatch::DataTreeTrackMatch() :
		fNCommonPoints(0),
		fRecTrackId(0),
		fMCTrackId(0)
{}

DataTreeTrackMatch::~DataTreeTrackMatch() {
	fDisplacements.clear();
}

