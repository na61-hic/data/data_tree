// Updated 12.10.2017 - Viktor


#ifndef DataTreeEvent_H
#define DataTreeEvent_H 1

#include <vector>

#include "TClonesArray.h"
#include "TObject.h"

#include "DataTreeTrack.h"
#include "DataTreePSDModule.h"
#include "DataTreeTOFHit.h"
#include "DataTreeV0Candidate.h"
#include "DataTreeMCTrack.h"
#include "DataTreeTrigger.h"
#include "DataTreeBPD.h"
#include "DataTreeWFA.h"
#include "DataTreeTrackMatch.h"
#include "DataTreePSDPrimaryParticle.h"

class DataTreeMCTrack;

class DataTreeTrackMatch;

const int fNumberOfV0Types = 3;
const int V0Pdg[fNumberOfV0Types] = {3122, -3122, 310};

class DataTreeEvent : public TObject {

public:

    DataTreeEvent();

    DataTreeEvent(const DataTreeEvent &dataTreeEvent);

    ~DataTreeEvent();

    void ClearEvent();

    void Process();

    // bool GetProcessFlag(){ return ProcessFlag; }

    int GetRunId() const { return fRunId; }

    int GetEventId() const { return fEventId; }

    double GetEventTimestamp() const { return fEventTimestamp; }

    double GetRPAngle() const { return fRPAngle; }

    double GetImpactParameter() const { return fImpactParameter; }

    double GetVertexPositionComponent(int idx, int jdx = 0) const { return fVertexPosition[jdx][idx]; }

    double GetVertexQuality(int idx = 0) const { return fVertexQuality[idx]; }

    int GetNFiredPSDModules() {
        Process();
        return fNumberOfFiredPSDModules;
    }

    int GetNFiredPSDSections() {
        Process();
        return fNumberOfFiredPSDSections;
    }

    double GetPSDEnergy() const { /*Process();*/ return fPSDEnergy; }

    double GetMCPSDEnergy() { /*Process();*/ return fMCPSDEnergy; }

    double GetMCVertexPositionComponent(int idx) const { return fMCVertexPosition[idx]; }

    int GetNV0Types() const { return fNumberOfV0Types; }

    int GetNV0Pdg(int idx) const { return V0Pdg[idx]; }

    void SetRunId(int fValue) { fRunId = fValue; }

    void SetEventId(int fValue) { fEventId = fValue; }

    void SetEventTimestamp(double fValue) { fEventTimestamp = fValue; }

    void SetRPAngle(double fValue) { fRPAngle = fValue; }

    void SetImpactParameter(double fValue) { fImpactParameter = fValue; }

    void SetVertexPosition(double fX, double fY, double fZ, int idx = 0) {
        fVertexPosition[idx][0] = fX;
        fVertexPosition[idx][1] = fY;
        fVertexPosition[idx][2] = fZ;
    }

    void SetVertexPositionComponent(int idx, double fValue, int jdx = 0) { fVertexPosition[jdx][idx] = fValue; }

    void SetVertexQuality(double fValue, int idx = 0) { fVertexQuality[idx] = fValue; }

    void SetHasVertex(bool pValue = true, int idx = 0) { fHasVertex[idx] = pValue; }

    void SetMCVertexPosition(double fX, double fY, double fZ) {
        fMCVertexPosition[0] = fX;
        fMCVertexPosition[1] = fY;
        fMCVertexPosition[2] = fZ;
    }

    void SetMCVertexPositionComponent(int idx, double fValue) { fMCVertexPosition[idx] = fValue; }

    void SetPsdEnergy(Double_t e) { fPSDEnergy = e; }

    void SetNV0SpecificCandidatesTOFpid(int idx, int fValue) { fNumberOfV0SpecificCandidatesTOFpid[idx] = fValue; }

    void SetNV0CandidatesTOFpid(int fValue) { fNumberOfV0CandidatesTOFpid = fValue; }

    void SetNV0SpecificCandidatesMCpid(int idx, int fValue) { fNumberOfV0SpecificCandidatesMCpid[idx] = fValue; }

    void SetNV0CandidatesMCpid(int fValue) { fNumberOfV0CandidatesMCpid = fValue; }

// // TODO check if we need it
    int GetNVertexTracks() const { return fNumberOfVertexTracks; }

    DataTreeTrack *GetVertexTrack(int idx) const { return (DataTreeTrack *) fVertexTracks->At(idx); }

    DataTreeTrack *GetLastVertexTrack() { return (DataTreeTrack *) fVertexTracks->At(fNumberOfVertexTracks - 1); }

    void AddVertexTrack();

    void ClearVertexTrack(int idx) { GetVertexTrack(idx)->SetUndefinedValues(); }
// //

    int GetNTracks() const { return fNumberOfTracks; }

    DataTreeTrack *GetTrack(int idx) const { return (DataTreeTrack *) fTracks->At(idx); }

    DataTreeTrack *GetLastTrack() { return (DataTreeTrack *) fTracks->At(fNumberOfTracks - 1); }

    void AddTrack();

    void ClearTrack(int idx) { GetTrack(idx)->SetUndefinedValues(); }


    double GetMCPSDEnergy() const { return fMCPSDEnergy; }

    void SetMCPSDEnergy(double mcpsdEnergy) { fMCPSDEnergy = mcpsdEnergy; }

    int GetNMCPSDModules() const { return fNumberOfMCPSDModules; }

    DataTreePSDModule *GetMCPSDModule(int idx) const { return (DataTreePSDModule *) fMCPSDModules->At(idx); }

    DataTreePSDModule *GetLastMCPSDModule() {
        return (DataTreePSDModule *) fMCPSDModules->At(fNumberOfMCPSDModules - 1);
    }

    void AddMCPSDModule() {
        TClonesArray &arr = *fMCPSDModules;
        new(arr[fNumberOfMCPSDModules]) DataTreePSDModule(fNumberOfMCPSDModules);
        fNumberOfMCPSDModules++;
    }

    void AddMCPSDModule(int inSections) {
        TClonesArray &arr = *fMCPSDModules;
        new(arr[fNumberOfMCPSDModules]) DataTreePSDModule(fNumberOfMCPSDModules, inSections);
        fNumberOfMCPSDModules++;
    }

    void ClearMCPSDModules() {
        fNumberOfMCPSDModules = 0;
        fMCPSDModules->Clear();
    }

    int GetNPSDModules() const { return fNumberOfPSDModules; }

    DataTreePSDModule *GetPSDModule(int idx) const { return (DataTreePSDModule *) fPSDModules->At(idx); }

    DataTreePSDModule *GetLastPSDModule() { return (DataTreePSDModule *) fPSDModules->At(fNumberOfPSDModules - 1); }

    void AddPSDModule() {
        TClonesArray &arr = *fPSDModules;
        new(arr[fNumberOfPSDModules]) DataTreePSDModule(fNumberOfPSDModules);
        fNumberOfPSDModules++;
    }

    void AddPSDModule(int inSections) {
        TClonesArray &arr = *fPSDModules;
        new(arr[fNumberOfPSDModules]) DataTreePSDModule(fNumberOfPSDModules, inSections);
        fNumberOfPSDModules++;
    }

    void ClearPSDModules() {
        fNumberOfPSDModules = 0;
        fPSDModules->Clear();
    }

    int GetNTOFHits() const { return fNumberOfTOFHits; }

    DataTreeTOFHit *GetTOFHit(int idx) const { return (DataTreeTOFHit *) fTOFHits->At(idx); }

    DataTreeTOFHit *GetLastTOFHit() { return (DataTreeTOFHit *) fTOFHits->At(fNumberOfTOFHits - 1); }

    // void AddTOFHit(){ TClonesArray &arr = *fTOFHits; new(arr[fNumberOfTOFHits]) DataTreeTOFHit(fNumberOfTOFHits); fNumberOfTOFHits++; }
    DataTreeTOFHit *AddTOFHit() {
        TClonesArray &arr = *fTOFHits;
        new(arr[fNumberOfTOFHits]) DataTreeTOFHit(fNumberOfTOFHits);
        fNumberOfTOFHits++;
        return (DataTreeTOFHit *) fTOFHits->At(fNumberOfTOFHits - 1);
    }

    int GetNV0SpecificCandidatesTOFpid(int idx) const { return fNumberOfV0SpecificCandidatesTOFpid[idx]; }

    int GetNV0CandidatesTOFpid() const { return fNumberOfV0CandidatesTOFpid; }

    DataTreeV0Candidate *GetV0CandidateTOFpid(int idx) const {
        return (DataTreeV0Candidate *) fV0CandidatesTOFpid->At(idx);
    }

    DataTreeV0Candidate *GetLastV0CandidateTOFpid() {
        return (DataTreeV0Candidate *) fV0CandidatesTOFpid->At(fNumberOfV0CandidatesTOFpid - 1);
    }

    DataTreeV0Candidate *AddV0CandidateTOFpid() {
        TClonesArray &arr = *fV0CandidatesTOFpid;
        new(arr[fNumberOfV0CandidatesTOFpid]) DataTreeV0Candidate(fNumberOfV0CandidatesTOFpid);
        fNumberOfV0CandidatesTOFpid++;
        return (DataTreeV0Candidate *) fV0CandidatesTOFpid->At(fNumberOfV0CandidatesTOFpid - 1);
    }

    int GetNV0SpecificCandidatesMCpid(int idx) const { return fNumberOfV0SpecificCandidatesMCpid[idx]; }

    int GetNV0CandidatesMCpid() const { return fNumberOfV0CandidatesMCpid; }

    DataTreeV0Candidate *GetV0CandidateMCpid(int idx) const {
        return (DataTreeV0Candidate *) fV0CandidatesMCpid->At(idx);
    }

    DataTreeV0Candidate *GetLastV0CandidateMCpid() {
        return (DataTreeV0Candidate *) fV0CandidatesMCpid->At(fNumberOfV0CandidatesMCpid - 1);
    }

    DataTreeV0Candidate *AddV0CandidateMCpid() {
        TClonesArray &arr = *fV0CandidatesMCpid;
        new(arr[fNumberOfV0CandidatesMCpid]) DataTreeV0Candidate(fNumberOfV0CandidatesMCpid);
        fNumberOfV0CandidatesMCpid++;
        return (DataTreeV0Candidate *) fV0CandidatesMCpid->At(fNumberOfV0CandidatesMCpid - 1);
    }

    int GetNMCTracks() const { return fNumberOfMCTracks; }

    DataTreeMCTrack *GetMCTrack(int idx) const { return (DataTreeMCTrack *) fMCTracks->At(idx); }

    DataTreeMCTrack *GetLastMCTrack() { return (DataTreeMCTrack *) fMCTracks->At(fNumberOfMCTracks - 1); }

    void AddMCTrack();

    int GetNTriggers() const { return fNumberOfTriggers; }

    DataTreeTrigger *GetTrigger(int idx) const { return (DataTreeTrigger *) fTriggers->At(idx); }

    DataTreeTrigger *GetLastTrigger() { return (DataTreeTrigger *) fTriggers->At(fNumberOfTriggers - 1); }

    void AddTrigger() {
        TClonesArray &arr = *fTriggers;
        new(arr[fNumberOfTriggers]) DataTreeTrigger(fNumberOfTriggers);
        fNumberOfTriggers++;
    }

    int GetNBPDs() const { return fNumberOfBPDs; }

    DataTreeBPD *GetBPD(int idx) const { return (DataTreeBPD *) fBPDs->At(idx); }

    DataTreeBPD *GetLastBPD() { return (DataTreeBPD *) fBPDs->At(fNumberOfBPDs - 1); }

    void AddBPD() {
        TClonesArray &arr = *fBPDs;
        new(arr[fNumberOfBPDs]) DataTreeBPD(fNumberOfBPDs);
        fNumberOfBPDs++;
    }

    int GetNWFAs() const { return fNumberOfWFAs; }

    DataTreeWFA *GetWFA(int idx) const { return (DataTreeWFA *) fWFAs->At(idx); }

    DataTreeWFA *GetLastWFA() { return (DataTreeWFA *) fWFAs->At(fNumberOfWFAs - 1); }

    void AddWFA() {
        TClonesArray &arr = *fWFAs;
        new(arr[fNumberOfWFAs]) DataTreeWFA(fNumberOfWFAs);
        fNumberOfWFAs++;
    }

    void AddTrackMatch(DataTreeTrack *recTrack, DataTreeMCTrack *mcTrack, Int_t nCommonPoints = 0,
                       Double_t recTrackPurity = 0, std::vector<double> displacements = std::vector<double>());

    Int_t GetNTrackMatches() const { return fTrackMatches->GetEntries(); }

    DataTreeTrackMatch *GetTrackMatch(Int_t idx) const { return (DataTreeTrackMatch *) fTrackMatches->At(idx); }


    void SetPsdPosition(double x, double y, double z) {
        fPsdPosition[0] = x;
        fPsdPosition[0] = y;
        fPsdPosition[0] = z;
    }

    void SetPsdPosition(const TVector3 &psdPosition) {
        SetPsdPosition(psdPosition.X(), psdPosition.Y(), psdPosition.Z());
    }

    double GetPsdPositionX() const { return fPsdPosition[0]; }

    double GetPsdPositionY() const { return fPsdPosition[1]; }

    double GetPsdPositionZ() const { return fPsdPosition[2]; }

    void SetBeamMomentum(double x, double y, double z) {
        fBeamMomentum[0] = x;
        fBeamMomentum[1] = y;
        fBeamMomentum[2] = z;
    }

    void SetBeamStatus(int status) { fBeamStatus = status; }

    double GetBeamMomentumX() const { return fBeamMomentum[0]; }

    double GetBeamMomentumY() const { return fBeamMomentum[1]; }

    double GetBeamMomentumZ() const { return fBeamMomentum[2]; }

    int GetBeamStatus() const { return fBeamStatus; }

    bool GetHasVertex(int idx) { return fHasVertex[idx]; }

    DataTreePSDPrimaryParticle *AddPSDPrimaryParticle() {
        fPSDPrimaryParticles->ExpandCreate(fPSDPrimaryParticles->GetEntries() + 1);
        return (DataTreePSDPrimaryParticle *) fPSDPrimaryParticles->Last();
    }

    const TClonesArray *GetPSDPrimaryParticles() const {
        return fPSDPrimaryParticles;
    }

    Int_t GetNPSDPrimaryParticles() const {
        return fPSDPrimaryParticles->GetEntries();
    }

    DataTreeEvent &operator=(const DataTreeEvent &dataTreeEvent);

protected:
    void copy(const DataTreeEvent &other);

    void free();

    bool ProcessFlag;

    // Event info

    int fRunId;
    int fEventId;
    double fEventTimestamp;
    double fVertexPosition[2][3];    //Position of the vertex
    bool fHasVertex[2];    //Position of the vertex
    double fVertexQuality[2];        //Quality of vertex fit
    double fBeamMomentum[3];
    int fBeamStatus;

    // MC-Event info

    double fMCVertexPosition[3];        //Position of the vertex in MC
    double fRPAngle;            //Reaction plane angle
    double fImpactParameter;        //Impact parameter

    double fMCPSDEnergy;            //sum of PSDEnergies for MC tracks
    int fNumberOfMCPSDModules;            //number of fPSD modules
    TClonesArray *fMCPSDModules;    //PSD modules


    int fNumberOfTracks;            //Number of tracks
    TClonesArray *fTracks;        //tracks

    int fNumberOfVertexTracks;            //Number of tracks
    TClonesArray *fVertexTracks;        //tracks

    // PSD info 

    int fNumberOfPSDModules;            //number of fPSD modules
    int fNumberOfFiredPSDModules;        //number of modules in PSD having some energy deposit
    int fNumberOfFiredPSDSections;        //number of sections in PSD having some energy deposit
    double fPsdPosition[3];
    double fPSDEnergy;            //total fPSD energy
    TClonesArray *fPSDModules;    //PSD modules

    // TOF info 

    int fNumberOfTOFHits;            //number of segments in TOF
    TClonesArray *fTOFHits;    //TOF segments

    // V0 info

    int fNumberOfV0CandidatesTOFpid;                //number of V0 candidates
    int fNumberOfV0SpecificCandidatesTOFpid[fNumberOfV0Types];    //number of V0 candidates of specific type
    TClonesArray *fV0CandidatesTOFpid;        //V0 candidates

    int fNumberOfV0CandidatesMCpid;            //number of V0 candidates with MC PiD
    int fNumberOfV0SpecificCandidatesMCpid[fNumberOfV0Types];    //number of V0 candidates wit MC PiD and of specific type
    TClonesArray *fV0CandidatesMCpid;        //V0 candidates with MC PiD

    // Triggers info

    int fNumberOfTriggers;            //Number of fNumberOfTriggers
    TClonesArray *fTriggers;        //Triggers

    int fNumberOfBPDs;                //Number of BPDs
    TClonesArray *fBPDs;        //BPDs

    int fNumberOfWFAs; //Number of WFA
    TClonesArray *fWFAs; //WFAs

    // MC-tracks info
    int fNumberOfMCTracks;            //Multiplicity of MC tracks
    TClonesArray *fMCTracks;        //MC tracks

    TClonesArray *fTrackMatches;

    TClonesArray *fPSDPrimaryParticles;


ClassDefNV(DataTreeEvent, 4)

};

#endif
