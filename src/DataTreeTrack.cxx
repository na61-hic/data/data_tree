#include "DataTreeTrack.h"

DataTreeTrack::DataTreeTrack(UInt_t idx) : TObject()
{
    SetId(idx);
    SetUndefinedValues();

}
DataTreeTrack::~DataTreeTrack()
{
    
}

ClassImp(DataTreeTrack)

void DataTreeTrack::SetUndefinedValues()
{
//     fParams.clear();
    fMomentum.SetXYZM(EnumGlobalConst::kUndefinedValue, EnumGlobalConst::kUndefinedValue, EnumGlobalConst::kUndefinedValue, 0);
    fCharge = EnumGlobalConst::kUndefinedValue;
    fNDF = EnumGlobalConst::kUndefinedValue;
    fChi2 = EnumGlobalConst::kUndefinedValue;
    fVtxChi2 = EnumGlobalConst::kUndefinedValue;
    fFlag = EnumGlobalConst::kUndefinedValue;

    for (int i=0; i<nSubDetectors; ++i)
        fDCA[i] = fNumberOfHits[i] = fNumberOfHitsPotential[i] = fdEdx[i] = fNumberOfdEdxClusters[i] = EnumGlobalConst::kUndefinedValue;

    fLength = EnumGlobalConst::kUndefinedValue;
    fPSDModuleId = EnumGlobalConst::kUndefinedValue;		//id of the corresponding PSD module
    fTOFHitId = EnumGlobalConst::kUndefinedValue;		//id of the corresponding TOF hit
    fMCTrackId = EnumGlobalConst::kUndefinedValue;		//id of the best matched MC track
    fType = EnumGlobalConst::kUndefinedValue;			//to distinguish different tracks (for example, main vertex and primary vertex tracks in NA61)    

}
void DataTreeTrack::SetMomentum(const TLorentzVector &value) {
    fMomentum.SetPxPyPzE(
        value.Px(),
        value.Py(),
        value.Pz(),
        value.Energy()
        );
}
