#include "DataTreeMCTrack.h"

ClassImp(DataTreeMCTrack)

DataTreeMCTrack::DataTreeMCTrack(UInt_t idx) : TObject()
{
    SetId(idx);
    SetUndefinedValues();
}

DataTreeMCTrack::~DataTreeMCTrack()
{
    
}

void DataTreeMCTrack::SetUndefinedValues()
{
    fMomentum.SetXYZM(EnumGlobalConst::kUndefinedValue, EnumGlobalConst::kUndefinedValue, EnumGlobalConst::kUndefinedValue, 0);
    fCharge = EnumGlobalConst::kUndefinedValue;
    
    fMotherId = EnumGlobalConst::kUndefinedValue;
    fPSDEnergy = 0;
    fPSDSectionId = EnumGlobalConst::kUndefinedValue;
    fTOFSegmentId = EnumGlobalConst::kUndefinedValue;
    
    for (int i=0; i<3; ++i)
        fPSDPosition[i] = fTOFPosition[i] = EnumGlobalConst::kUndefinedValue;

    for (int i=0; i<nSubDetectors; ++i)
        fNumberOfHits[i] = EnumGlobalConst::kUndefinedValue;

}
void DataTreeMCTrack::SetMomentum(const TLorentzVector &value) {
    fMomentum.SetPxPyPzE(
        value.Px(),
        value.Py(),
        value.Pz(),
        value.Energy()
        );
}


