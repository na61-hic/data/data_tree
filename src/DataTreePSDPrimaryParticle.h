//
// Created by eugene on 9/16/18.
//

#ifndef DATATREE_DATATREEPSDPRIMARYPARTICLE_H
#define DATATREE_DATATREEPSDPRIMARYPARTICLE_H


#include <TObject.h>
#include <TLorentzVector.h>

class DataTreePSDPrimaryParticle : public TObject {

public:
    DataTreePSDPrimaryParticle() :
            TObject(),
            fZ(0),
            fA(0),
            fPdgID(0),
            fPsdModuleId(-1),
            fVisibleELoss(-1.)
            {};

    DataTreePSDPrimaryParticle(const DataTreePSDPrimaryParticle &dataTreePSDPrimaryParticle) :
            TObject(dataTreePSDPrimaryParticle),
            fZ(dataTreePSDPrimaryParticle.fZ),
            fA(dataTreePSDPrimaryParticle.fA),
            fPdgID(dataTreePSDPrimaryParticle.fPdgID),
            fMomentum(dataTreePSDPrimaryParticle.fMomentum),
            fPosition(dataTreePSDPrimaryParticle.fPosition),
            fPsdModuleId(-1),
            fVisibleELoss(-1.)
            {

    };

    Int_t GetZ() const {
        return fZ;
    }

    void SetZ(Int_t z) {
        DataTreePSDPrimaryParticle::fZ = z;
    }

    Int_t GetA() const {
        return fA;
    }

    void SetA(Int_t a) {
        DataTreePSDPrimaryParticle::fA = a;
    }

    Int_t GetType() const {
        return fType;
    }

    void SetType(Int_t type) {
        DataTreePSDPrimaryParticle::fType = type;
    }

    Long64_t GetPdgID() const {
        return fPdgID;
    }

    void SetPdgID(Long64_t pdgID) {
        DataTreePSDPrimaryParticle::fPdgID = pdgID;
    }

    const TLorentzVector &GetMomentum() const {
        return fMomentum;
    }

    void SetMomentum(const TLorentzVector &momentum) {
        DataTreePSDPrimaryParticle::fMomentum = momentum;
    }

    const TVector3 &GetPosition() const {
        return fPosition;
    }

    void SetPosition(const TVector3 &position) {
        DataTreePSDPrimaryParticle::fPosition = position;
    }


    Int_t GetPsdModuleId() const {
        return fPsdModuleId;
    }

    void SetPsdModuleId(Int_t psdModuleId) {
        DataTreePSDPrimaryParticle::fPsdModuleId = psdModuleId;
    }

    Double32_t GetVisibleELoss() const {
        return fVisibleELoss;
    }

    void SetVisibleELoss(Double32_t visibleELoss) {
        DataTreePSDPrimaryParticle::fVisibleELoss = visibleELoss;
    }

protected:
    Int_t fZ; // particle charge
    Int_t fA; // particle atomic number
    Int_t fType; // particle type: ion, hadron, etc
    Long64_t fPdgID; // pdg code

    TLorentzVector fMomentum; // particle momentum
    TVector3 fPosition; // particle position

    Int_t fPsdModuleId;
    Double32_t fVisibleELoss;

ClassDef(DataTreePSDPrimaryParticle, 2)
};


#endif //DATATREE_DATATREEPSDPRIMARYPARTICLE_H
