#ifndef DataTreeWFA_H
#define DataTreeWFA_H 1

#include <vector>
#include <iostream>
#include "TClonesArray.h"
#include "TObject.h"

class DataTreeWFA : public TObject
{
    
public:
  
    DataTreeWFA(int idx = 0);
    ~DataTreeWFA();
        
    int GetId() const {return fId;}
    
    unsigned long GetNHits() const {return fTimesWFA.size();}
    std::vector <double> GetTimesWFA () const { return fTimesWFA; }
    void SetTimesWFA(std::vector <double> times) { fTimesWFA = times; }
    
private:    
    void SetId(int idx){fId = idx;}
    
    std::vector <double> fTimesWFA;
    int   fId;
    
    ClassDefNV(DataTreeWFA, 1)
};

#endif
