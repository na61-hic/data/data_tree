#include "DataTreePSDModule.h"

DataTreePSDModule::DataTreePSDModule(int idx) : TObject(),
fProcessFlag(false),
// fSections (new TClonesArray("DataTreePSDSection")),
fSections (0),
fNumberOfSections(0)
{
    SetId(idx);
}

DataTreePSDModule::DataTreePSDModule(int idx, int inSections) : TObject(),
fProcessFlag(false),
// fSections (new TClonesArray("DataTreePSDSection")),
fSections (0),
fNumberOfSections(0)
{
    SetId(idx);
    for (int i=0;i<inSections;i++)
	AddSection();
    
    fNumberOfSections = inSections;
}

void DataTreePSDModule::ClearEvent()
{
    for(int i=0;i<fNumberOfSections;i++)
        GetSection(i)->ClearEvent();
    
    fSections.clear();
    fEnergy = 0.;
    fProcessFlag = false;
    fPosition[0] = fPosition[1] = fPosition[2] = EnumGlobalConst::kUndefinedValue;
}

void DataTreePSDModule::Process()
{
    fEnergy = 0.;
    for(int i=0;i<fNumberOfSections;i++)
        fEnergy += GetSection(i)->GetEnergy();
    
    fProcessFlag = true;
}

DataTreePSDModule::~DataTreePSDModule()
{
    
}

ClassImp(DataTreePSDModule)

