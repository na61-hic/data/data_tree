#ifndef DataTreeTOFHit_H
#define DataTreeTOFHit_H 1

#include <vector>
#include <iostream>
#include "TClonesArray.h"
#include "TObject.h"

#include "DataTreeConstants.h"      

class DataTreeTOFHit : public TObject
{
    
public:
  
    DataTreeTOFHit(int idx = 0);
    ~DataTreeTOFHit();
    
    void ClearEvent(){ fTime = fPosition[0] = fPosition[1] = fPosition[2] = EnumGlobalConst::kUndefinedValue; }
    
    int GetId(){return fId;}
    
    float GetPositionComponent(int idx) const {return fPosition[idx];}
    float GetX() const {return fPosition[0];}
    float GetY() const {return fPosition[1];}
    float GetZ() const {return fPosition[2];}
    float GetTime() const {return fTime;}
    std::vector <int> GetRecoTrackId() const { return fRecoTrackId; }
    int GetFirstRecoTrackId() const { return fRecoTrackId.at(0); }
        
    void SetId(const int idx){fId = idx;}
    void SetPositionComponent(const int idx, const float value){fPosition[idx]=value;}
    void SetPosition(const float x, const float y, const float z){fPosition[0]=x; fPosition[1]=y; fPosition[2]=z;}
    void SetTime(const float t) { fTime = t; }
    void SetRecoTrackId(const std::vector <int>& id) { fRecoTrackId = id; }
    void AddRecoTrackId(const int id) { fRecoTrackId.push_back(id); }
    
    void SetPathLength(const float value) { fPathLength = value;} 
    void SetCharge(const float value) { fCharge = value;} 
    void SetSquaredMass(const float value) { fSquaredMass = value;} 
    void SetSquaredMassError(const float value) { fSquaredMassError = value;} 

    float GetPathLength(void) const { return fPathLength; } 
    float GetCharge(void) const { return fCharge; } 
    float GetBeta(void) const { return fPathLength / fTime / (float)SpeedOfLight; }
    float GetSquaredMass(void) const { return fSquaredMass; } 
    float GetSquaredMassError(void) const { return fSquaredMassError; } 
   
    int GetStatus() const { return fStatus; }
    void SetStatus(const int value) { fStatus = value; }
    
private:    
    
    int fId;
    int fStatus;
    std::vector <int> fRecoTrackId;
    float fPosition[3];
    float fTime;
    
    float fPathLength;
    float fCharge;
    float fSquaredMass;
    float fSquaredMassError;
    
    
    ClassDefNV(DataTreeTOFHit, 1)
};

#endif
