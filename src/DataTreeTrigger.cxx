#include "DataTreeTrigger.h"

DataTreeTrigger::DataTreeTrigger(int idx) : TObject()
{
    fSignal = EnumGlobalConst::kUndefinedValue;
    fIsFired = false;
    SetId(idx);
}

DataTreeTrigger::~DataTreeTrigger()
{
    
}

ClassImp(DataTreeTrigger)

