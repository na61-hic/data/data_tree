#ifndef DataTreeConstants_H
#define DataTreeConstants_H 1

#include <DataTreeVersion.h>

const double SpeedOfLight = 29.9792458;

#ifdef DATATREE_SHINE

namespace EnumTrigger {
enum eTriggerId {
  kS1 = 0,
  kS2 = 1,
  kS3 = 2,
  kV1 = 3,
  kPSD = 4,
  kT1 = 5,
  kT2 = 6,
  kT4 = 7,
  kV1p = 8,
  nTriggers = 9
};
}

namespace EnumParticles {
enum eParticle {
  kProton = 0,
  kPion,
  kKaon,
  nParticles
};
}

namespace EnumCentrality {
enum eCentrality {
  kTracks = 0,
  kPsd,
  nCentrality
};
}

namespace EnumWFATimeWindow {
enum eWFATimeWindow {
  kS1 = -1,
  kS2 = -1,
  kS3 = -1,
  kV1 = -1,
  kPSD = -1,
  kT1 = 4000,
  kT2 = -1,
  kT4 = 25000
};
}

namespace EnumVertexType {
enum eVertexType {
  kReconstructedVertex = 0,
  kDBVertex = 1,
  nVertexTypes = 2
};
}

namespace EnumWFA {
enum eWFA {
  kS1_1 = 0,
  kT4 = 1,
  nWFATypes = 2
};
}

namespace EnumBPD {
enum eBPD {
  kBPD1 = 0,
  kBPD2 = 1,
  kBPD3 = 2,
  nBPDs = 3,
};
}

namespace EnumTPC {
enum eTPC {
  kVTPC1 = 0,
  kVTPC2,
  kMTPC,
  kTPCAll,
  nTPCs
};
}
namespace EnumGlobalConst {
enum eGlobalConst {
  kUndefinedValue = -999
};
}

namespace EnumParamsPoint {
enum eParamsPoint {
//         kVertex = 0,
      kFirstHit = 0,
//         kLastHit = 2,
      nParamsPoints
};
}

#endif

#ifdef DATATREE_CBM

namespace EnumTrigger{
    enum eTriggerId{
        kS1 = 0,
        kS2 = 1,
        kS3 = 2,
        kV1 = 3,
        kPSD= 4,
        kT1 = 5,
        kT2 = 6,
        kT4 = 7,
        kV1p = 8,
        nTriggers = 9
    };
}

namespace EnumParticles{
    enum eParticle{
        kProton = 0,
        kPion,
        kKaon,
        nParticles
    };
}

namespace EnumCentrality{
    enum eCentrality{
        kTracks = 0,
        kPsd,
        nCentrality
    };
}


namespace EnumWFATimeWindow{
    enum eWFATimeWindow{
        kS1 = -1,
        kS2 = -1,
        kS3 = -1,
        kV1 = -1,
        kPSD= -1,
        kT1 = 4000,
        kT2 = -1,
        kT4 = 25000
    };
}

namespace EnumVertexType{
    enum eVertexType{
        kReconstructedVertex = 0,
        kDBVertex = 1,
        nVertexTypes = 2
    };
}

namespace EnumWFA{
    enum eWFA{
        kS1_1 = 0,
        kT4 = 1,
        nWFATypes = 2
    };
}

namespace EnumBPD{
    enum eBPD{
        kBPD1 = 0,
        kBPD2 = 1,
        kBPD3 = 2,
        nBPDs = 3,
    };
}

namespace EnumTPC{
    enum eTPC{
        kVTPC1 = 0,
        kVTPC2,
        kMTPC,
        kTPCAll,
        nTPCs
    };
}
namespace EnumGlobalConst{
    enum eGlobalConst{
        kUndefinedValue = -999
    };
}

namespace EnumParamsPoint{
    enum eParamsPoint{
        kVertex = 0,
        kTof,
        nParamsPoints
    };
}

#endif

#endif



