#ifndef DataTreeV0Candidate_H
#define DataTreeV0Candidate_H 1

#include <vector>
#include <iostream>
#include "TClonesArray.h"
#include "TObject.h"
#include "TLorentzVector.h"

class DataTreeV0Candidate : public TObject
{
    
public:
  
    DataTreeV0Candidate(int idx = 0);
    ~DataTreeV0Candidate();
    
    int GetId(){return fId;}
    
    int GetPdgId() const {return fPdgId;}
    double GetChi2() const {return fChi2;}
    double GetCharge() const {return fCharge;}
    int GetTrackId() const {return fTrackId;}
    int GetNumberOfDaughters() const {return fNumberOfDaughters;}
    double GetPt() const { return fMomentum.Pt(); }
    double GetPhi() const { return fMomentum.Phi(); }
    double GetEta() const { return fMomentum.Eta(); }
    double GetPx() const { return fMomentum.Px(); }
    double GetPy() const { return fMomentum.Py(); }
    double GetPz() const { return fMomentum.Pz(); }
    double GetP() const { return fMomentum.P(); }
    double GetRapidity() const { return fMomentum.Rapidity(); }
    double GetEnergy() const { return fMomentum.E(); }
    TLorentzVector GetMomentum() const { return fMomentum; }
    
    void SetPdgId(int value){fPdgId = value;}
    void SetChi2(double chi2){fChi2 = chi2;}
    void SetCharge(double charge){fCharge = charge;}
    void SetTrackId(int idx){ fTrackId = idx;}
    void SetMomentum(TLorentzVector mom) { fMomentum = mom; } 
    
    DataTreeV0Candidate* GetDaughter(int idx){return (DataTreeV0Candidate*)fDaughters->At(idx);}
    DataTreeV0Candidate* GetLastDaughter(){return (DataTreeV0Candidate*)fDaughters->At(fNumberOfDaughters-1);}
    
    void AddDaughter(){
        TClonesArray &arr = *fDaughters; 
        new(arr[fNumberOfDaughters]) DataTreeV0Candidate(fNumberOfDaughters); 
        fNumberOfDaughters++;
    }
    
private:
    
    void SetId(int idx){fId = idx;}
    
    int fId;
    TLorentzVector fMomentum;
    
    int fPdgId;		        //pdg
    double fChi2;		//Chi squared 
    double fCharge;		//charge
    
    int fTrackId;		//id of the corresponding track

    int fNumberOfDaughters;		//Number of daughter tracks
    TClonesArray* fDaughters;	//Daughter tracks 
    
    ClassDefNV(DataTreeV0Candidate, 1)
};

#endif
