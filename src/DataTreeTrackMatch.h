/*
 * DataTreeTrackMatch.h
 *
 *  Created on: Mar 1, 2018
 *      Author: eugene
 */

#ifndef INCLUDE_DATATREETRACKMATCH_H_
#define INCLUDE_DATATREETRACKMATCH_H_

#include <TObject.h>
#include <TRef.h>

#include "DataTreeEvent.h"
#include "DataTreeTrack.h"
#include "DataTreeMCTrack.h"


class DataTreeMCTrack;

class DataTreeTrackMatch: public TObject {
public:
	DataTreeTrackMatch();
	virtual ~DataTreeTrackMatch();


	const std::vector<double>& GetDisplacements() const {
		return fDisplacements;
	}

	UInt_t GetMcTrackId() const {
		return fMCTrackId;
	}

	Int_t GetNCommonPoints() const {
		return fNCommonPoints;
	}

	UInt_t GetRecTrackId() const {
		return fRecTrackId;
	}

	Double_t GetRecTrackPurity() const {
		return fRecTrackPurity;
	}

	DataTreeMCTrack *GetMCTrack() const {
		return (DataTreeMCTrack*) fMCTrack.GetObject();
	}

	DataTreeTrack *GetRecTrack() const {
		return (DataTreeTrack*) fRecTrack.GetObject();
	}


	friend class DataTreeMCTrack;
    friend class DataTreeEvent;

protected:
	Int_t fNCommonPoints;
	Double_t fRecTrackPurity;
	std::vector<double> fDisplacements;


	UInt_t fMCTrackId;
	UInt_t fRecTrackId;

	TRef fMCTrack;
	TRef fRecTrack;

ClassDef(DataTreeTrackMatch, 1);
};

#endif /* INCLUDE_DATATREETRACKMATCH_H_ */
