#ifndef DataTreeTrack_H
#define DataTreeTrack_H 1

#include <Rtypes.h>

#include <vector>
#include <iostream>
#include <fstream>
#include "TClonesArray.h"
#include "TObject.h"
#include "TMath.h"
#include "TLorentzVector.h"

#include "DataTreeTrackParams.h"
#include "DataTreeConstants.h"      

class DataTreeTrack : public TObject
{
    
public:
  
enum STSTrackPoints
{
    kVtxPointTrack = 0,
    kFirstPointTrack,
    kLastPointTrack,
    kNPoints
};

    static const int nSubDetectors = 5;
  
    DataTreeTrack(UInt_t idx = 0);
    ~DataTreeTrack();

    void SetId(UInt_t idx){ fId = idx; }
    UInt_t GetId() const { return fId; }
    void SetUndefinedValues();

    void SetCharge(int value){ fCharge = value; }
    void SetNumberOfHits(const int value, const int subidx = 0){ fNumberOfHits[subidx] = value; }
    void SetNumberOfHitsPotential(const int value, const int subidx = 0){ fNumberOfHitsPotential[subidx] = value; }
    void SetdEdx(const double dEdx, const int subidx = 0){ fdEdx[subidx] = dEdx; }
    void SetNumberOfdEdxClusters(const double dEdx, const int subidx = 0){ fNumberOfdEdxClusters[subidx] = dEdx; }
    void SetFlag(const int Flag){ fFlag = Flag; }
    void SetChi2(const double chi2){ fChi2 = chi2; }
    void SetVtxChi2(const double value){ fVtxChi2 = value; }
    void SetNDF(const int NDF){ fNDF = NDF; }
    void SetDCA(const double X, const double Y, const double Z){ fDCA[0]=X; fDCA[1]=Y; fDCA[2]=Z; }
    void SetDCAComponent(const int idx, const double value){ fDCA[idx] = value; }
    void SetLength(const double value){ fLength = value; }
    void SetPSDModuleId(const int idx){ fPSDModuleId = idx; }
    void SetTOFHitId(const int idx){ fTOFHitId = idx; }
    void SetMCTrackId(const int idx){ fMCTrackId = idx; }
    void SetType(const int value){ fType = value; }
    void SetMomentum(const TLorentzVector& value);
    
    int GetCharge() const { return fCharge; }    
    int GetNumberOfHits (const int subidx = 0) const { return fNumberOfHits[subidx]; }
    int GetNumberOfHitsPotential (int subidx = 0) const { return fNumberOfHitsPotential[subidx]; }
    int GetFlag() const { return  fFlag; }
    int GetNDF() const { return  fNDF; }
    int GetPSDModuleId() const { return  fPSDModuleId; }
    int GetTOFHitId() const { return  fTOFHitId; }
    int GetMCTrackId() const { return  fMCTrackId; }
    int GetType() const { return  fType; }    
    double GetdEdx(const int subidx = 0) const { return  fdEdx[subidx]; }
    double GetNumberOfdEdxClusters(int subidx = 0) const { return  fNumberOfdEdxClusters[subidx]; }
    double GetChi2() const { return  fChi2; }
    double GetVtxChi2() const { return  fVtxChi2; }
    double GetDCAComponent(const int idx) const { return  fDCA[idx]; }
    double GetLength() const { return  fLength; }

    double GetPt() const { return fMomentum.Pt(); }
    double GetPhi() const { return fMomentum.Phi(); }
    double GetEta() const { return fMomentum.Eta(); }
    double GetPx() const { return fMomentum.Px(); }
    double GetPy() const { return fMomentum.Py(); }
    double GetPz() const { return fMomentum.Pz(); }
    double GetP() const { return fMomentum.P(); }
    double GetRapidity() const { return fMomentum.Rapidity(); }
    double GetEnergy() const { return fMomentum.E(); }
    
    TLorentzVector GetMomentum() const { return fMomentum; }

//     void AddParams(const DataTreeTrackParams data) { fParams.push_back(data); }
//     const DataTreeTrackParams& GetParams(const int i) const { return fParams.at(i); }

    void SetParams(const DataTreeTrackParams &data, int id) { fParams[id] = data; }
    const DataTreeTrackParams GetParams(const int i) const { return fParams[i]; }
//     unsigned int GetParamsNumber() const { return fParams.size(); }

	double GetTrackPurity() const {
		return fTrackPurity;
	}

	void SetTrackPurity(double trackPurity) {
		fTrackPurity = trackPurity;
	}
    
private:
    
    UInt_t fId;
    
//     std::vector <DataTreeTrackParams> fParams;
//     std::vector<DataTreeTrackParams*> fParams;
    DataTreeTrackParams fParams[EnumParamsPoint::nParamsPoints];
    
    TLorentzVector fMomentum;
    int fCharge;
    double fDCA[3];
    int fNDF;
    double fChi2;
    double fVtxChi2;
    int fFlag;
    
    int fNumberOfHits[nSubDetectors];
    int fNumberOfHitsPotential[nSubDetectors];
    double fdEdx[nSubDetectors];
    double fNumberOfdEdxClusters[nSubDetectors];
    double fLength;
    
    int fPSDModuleId;		//id of the corresponding PSD module
    int fTOFHitId;		//id of the corresponding TOF hit
    int fMCTrackId;		//id of the best matched MC track
    int fType;			//to distinguish different tracks (for example, main vertex and primary vertex tracks in NA61)
        

    double fTrackPurity; // track purity after track matching

    ClassDef(DataTreeTrack, 2)

};

#endif
