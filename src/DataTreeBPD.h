#ifndef DataTreeBPD_H
#define DataTreeBPD_H 1

#include <vector>
#include <TObject.h>

class DataTreeBPD : public TObject
{
    
public:
  
    DataTreeBPD(int fIdx = 0);
    ~DataTreeBPD();
        
    void SetPosition(double x, double y){fPosition[0]=x; fPosition[1]=y;}
    void SetPositionComponent(int idx, double fValue){fPosition[idx] = fValue;}

    void SetPositionError(double x, double y){fPositionError[0]=x; fPositionError[1]=y; }
    void SetPositionErrorComponent(int idx, double fValue){fPositionError[idx] = fValue;}

    void SetId(int idx){fId = idx;}
    void SetStatus(int sx, int sy) { fStatus[0] = sx; fStatus[1] = sy; }
    void SetZ(int idx, double value) { fZ[idx] = value; }
    void SetRMS(int idx, double value) { fRMS[idx] = value; }
    void SetMaximum(int idx, double value) { fMaximum[idx] = value; }
    void SetCharge(int idx, double value) { fCharge[idx] = value; }
    void SetSumOfAll(int idx, double value) { fSumOfAll[idx] = value; }
 
    int GetId() const { return fId; }
    int GetStatus(int idx) const { return fStatus[idx]; }    
    double GetZ(int idx) const { return fZ[idx]; }
    double GetRMS(int idx) const { return fRMS[idx]; }
    double GetMaximum(int idx) const { return fMaximum[idx]; }    
    double GetCharge(int idx) const { return fRMS[idx]; }    
    double GetSumOfAll(int idx) const { return fSumOfAll[idx]; }
    double GetPositionComponent(int idx) const {return fPosition[idx];}
    double GetPositionErrorComponent(int idx) const {return fPositionError[idx];}
    
private:    
    
    int fId;
    int fStatus[2];
    double fPosition[2];
    double fPositionError[2];
    double fZ[2];
    double fRMS[2];
    double fMaximum[2];
    double fCharge[2];
    double fSumOfAll[2];
    
    ClassDefNV(DataTreeBPD, 1);
};

#endif
