#ifndef DataTreePSDModule_H
#define DataTreePSDModule_H 1

#include <vector>
#include <iostream>
#include "TClonesArray.h"
#include "TObject.h"
#include "DataTreePSDSection.h"
#include "TMath.h"

#include "DataTreeConstants.h"      
#include "DataTreePSDSection.h"      

#include <vector>      

class DataTreePSDModule : public TObject
{
    
public:
  
    DataTreePSDModule(int idx = 0);
    DataTreePSDModule(int idx, int inSections);
    ~DataTreePSDModule();
    
    void ClearEvent();
    void Process();
    
    int GetId() const {return fId;}
    double GetPositionComponent(const int idx) const {return fPosition[idx];}
    double GetPhi() const {return TMath::ATan2(fPosition[1],fPosition[0]);}
    double GetEnergy() const  { /*Process();*/ return fEnergy; }
    int GetNSections() const {return fNumberOfSections;}
    
    void SetPosition(const double x, const double y, const double z){fPosition[0]=x; fPosition[1]=y; fPosition[2]=z;}
    void SetPositionComponent(const int idx, const double value){fPosition[idx] = value;}
    void SetEnergy(const double value){fEnergy = value;}
    
    DataTreePSDSection* GetSection(const int idx){return (DataTreePSDSection*)fSections.at(idx);}
    void AddSection(){ fSections.push_back( new DataTreePSDSection() ); fNumberOfSections++; }
    DataTreePSDSection* GetLastSection(){return (DataTreePSDSection*)fSections.at(fNumberOfSections-1);}
    
//     DataTreePSDSection* GetSection(const int idx){return (DataTreePSDSection*)fSections->At(idx);}
//     DataTreePSDSection* GetLastSection(){return (DataTreePSDSection*)fSections->At(fNumberOfSections-1);}
//     void AddSection(){
//         TClonesArray &arr = *fSections; 
//         new(arr[fNumberOfSections]) DataTreePSDSection(fNumberOfSections); 
//         fNumberOfSections++;
//     }
    
    void SetId(const int idx){fId = idx;}
    
private:    
  
    bool fProcessFlag;        
    int fId;
    double fPosition[3];			//Position of the module in lab frame
    double fEnergy;			//energy deposit in the module
    int fNumberOfSections;			//number of sections
//     TClonesArray* fSections;		//sections in the module
    std::vector <DataTreePSDSection*> fSections;		//sections in the module
    
    ClassDefNV(DataTreePSDModule, 1)
};

#endif
