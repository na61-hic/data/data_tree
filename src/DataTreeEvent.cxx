#include "DataTreeEvent.h"
#include "DataTreePSDPrimaryParticle.h"

DataTreeEvent::DataTreeEvent() : TObject(),
                                 ProcessFlag(false),
                                 fNumberOfTracks(0),
                                 fTracks(new TClonesArray("DataTreeTrack")),
                                 fNumberOfVertexTracks(0),
                                 fVertexTracks(new TClonesArray("DataTreeTrack")),
                                 fNumberOfMCPSDModules(0),
                                 fMCPSDModules(new TClonesArray("DataTreePSDModule")),
                                 fNumberOfPSDModules(0),
                                 fPSDModules(new TClonesArray("DataTreePSDModule")),
                                 fNumberOfTOFHits(0),
                                 fTOFHits(new TClonesArray("DataTreeTOFHit")),
                                 fNumberOfV0CandidatesTOFpid(0),
                                 fV0CandidatesTOFpid(new TClonesArray("DataTreeV0Candidate")),
                                 fNumberOfV0CandidatesMCpid(0),
                                 fV0CandidatesMCpid(new TClonesArray("DataTreeV0Candidate")),
                                 fNumberOfTriggers(0),
                                 fTriggers(new TClonesArray("DataTreeTrigger")),
                                 fNumberOfBPDs(0),
                                 fBPDs(new TClonesArray("DataTreeBPD")),
                                 fNumberOfWFAs(0),
                                 fWFAs(new TClonesArray("DataTreeWFA")),
                                 fNumberOfMCTracks(0),
                                 fMCTracks(new TClonesArray("DataTreeMCTrack")),
                                 fTrackMatches(new TClonesArray(DataTreeTrackMatch::Class())),
                                 fPSDPrimaryParticles(
                                         new TClonesArray(DataTreePSDPrimaryParticle::Class())) {
}

DataTreeEvent::DataTreeEvent(const DataTreeEvent &dataTreeEvent) :
        TObject(dataTreeEvent) {
    copy(dataTreeEvent);
}

DataTreeEvent &DataTreeEvent::operator=(const DataTreeEvent &dataTreeEvent) {
    if (&dataTreeEvent == this) {
        return *this;
    }

    free();
    TObject::operator=(dataTreeEvent);
    copy(dataTreeEvent);

    return *this;
}


void DataTreeEvent::AddMCTrack() {
    TClonesArray &arr = *fMCTracks;
    new(arr[fNumberOfMCTracks]) DataTreeMCTrack((UInt_t) fNumberOfMCTracks);
    fNumberOfMCTracks++;
}

void DataTreeEvent::AddVertexTrack() {
    TClonesArray &arr = *fVertexTracks;
    new(arr[fNumberOfVertexTracks]) DataTreeTrack(fNumberOfVertexTracks);
    fNumberOfVertexTracks++;
    ClearVertexTrack(fNumberOfVertexTracks - 1);
}

void DataTreeEvent::AddTrack() {
    TClonesArray &arr = *fTracks;
    new(arr[fNumberOfTracks]) DataTreeTrack(fNumberOfTracks);
    fNumberOfTracks++;
    ClearTrack(fNumberOfTracks - 1);
}

void DataTreeEvent::ClearEvent() {
    for (int i = 0; i < fNumberOfPSDModules; ++i) {
        GetPSDModule(i)->ClearEvent();
    }
    fPSDModules->Clear();
    for (int i = 0; i < 3; ++i) {
        fBeamMomentum[i] = EnumGlobalConst::kUndefinedValue;
        fVertexPosition[0][i] = fVertexPosition[1][i] = EnumGlobalConst::kUndefinedValue;
        fMCVertexPosition[i] = EnumGlobalConst::kUndefinedValue;
    }
    fRunId = EnumGlobalConst::kUndefinedValue;
    fEventId = EnumGlobalConst::kUndefinedValue;
    fEventTimestamp = EnumGlobalConst::kUndefinedValue;
    fHasVertex[0] = fHasVertex[1] = EnumGlobalConst::kUndefinedValue;           //Position of the vertex
    fVertexQuality[0] = fVertexQuality[1] = EnumGlobalConst::kUndefinedValue;        //Quality of vertex fit
    fBeamStatus = EnumGlobalConst::kUndefinedValue;

    fRPAngle = EnumGlobalConst::kUndefinedValue;            //Reaction plane angle
    fImpactParameter = EnumGlobalConst::kUndefinedValue;        //Impact parameter
    fMCPSDEnergy = EnumGlobalConst::kUndefinedValue;            //sum of PSDEnergies for MC tracks

    fPSDEnergy = 0.;
    fNumberOfFiredPSDModules = 0;
    fNumberOfFiredPSDSections = 0;
    fNumberOfPSDModules = 0;

    fTracks->Clear();
    fNumberOfTracks = 0;
    fVertexTracks->Clear();
    fNumberOfVertexTracks = 0;
    fMCTracks->Clear();
    fNumberOfMCTracks = 0;
    fV0CandidatesTOFpid->Clear();
    fNumberOfV0CandidatesTOFpid = 0;
    fV0CandidatesMCpid->Clear();
    fNumberOfV0CandidatesMCpid = 0;
    for (int i = 0; i < fNumberOfV0Types; ++i) {
        fNumberOfV0SpecificCandidatesTOFpid[i] = 0;
        fNumberOfV0SpecificCandidatesMCpid[i] = 0;
    }
    fTOFHits->Clear();
    fNumberOfTOFHits = 0;
    fBPDs->Clear();
    fNumberOfBPDs = 0;
    fTriggers->Clear();
    fNumberOfTriggers = 0;
    fWFAs->Clear();
    fNumberOfWFAs = 0;

    fTrackMatches->Clear();

    fPSDPrimaryParticles->Clear("C");

    ProcessFlag = false;
}


void DataTreeEvent::Process() {
    fPSDEnergy = 0.;
    fMCPSDEnergy = 0.;

    for (int i = 0; i < fNumberOfPSDModules; ++i) {
        fPSDEnergy += GetPSDModule(i)->GetEnergy();
    }
    for (int i = 0; i < fNumberOfMCTracks; ++i)
        fMCPSDEnergy += GetMCTrack(i)->GetPSDEnergy();


    ProcessFlag = true;
}


DataTreeEvent::~DataTreeEvent() {
    free();
}

void DataTreeEvent::AddTrackMatch(DataTreeTrack *recTrack, DataTreeMCTrack *mcTrack, Int_t nCommonPoints,
                                  Double_t recTrackPurity, std::vector<double> displacements) {

    DataTreeTrackMatch *match = dynamic_cast<DataTreeTrackMatch *>(fTrackMatches->ConstructedAt(
            fTrackMatches->GetEntries()));

    match->fRecTrack = recTrack;
    match->fMCTrack = mcTrack;
    match->fRecTrackId = recTrack->GetId();
    match->fMCTrackId = mcTrack->GetId();
    match->fNCommonPoints = nCommonPoints;
    match->fRecTrackPurity = recTrackPurity;
    match->fDisplacements = displacements;
}

void DataTreeEvent::free() {
    ClearEvent();
}

void DataTreeEvent::copy(const DataTreeEvent &other) {
    fRunId = other.fRunId;
    fEventId = other.fEventId;

    fEventTimestamp = other.fEventTimestamp;

    std::copy(&other.fVertexPosition[0][0], &other.fVertexPosition[0][0] + 2 * 3, &fVertexPosition[0][0]);

    std::copy(&other.fHasVertex[0], &other.fHasVertex[0] + 2, &fHasVertex[0]);

    std::copy(&other.fVertexQuality[0], &other.fVertexQuality[0] + 2, &fVertexQuality[0]);

    std::copy(&other.fBeamMomentum[0], &other.fBeamMomentum[0] + 3, &fBeamMomentum[0]);

    fBeamStatus = other.fBeamStatus;

    std::copy(&other.fMCVertexPosition[0], &other.fMCVertexPosition[0] + 3, &fMCVertexPosition[0]);

    fRPAngle = other.fRPAngle;

    fImpactParameter = other.fImpactParameter;

    fMCPSDEnergy = other.fMCPSDEnergy;

    fNumberOfMCPSDModules = other.fNumberOfMCPSDModules;
    *fMCPSDModules = *other.fMCPSDModules;

    fNumberOfTracks = other.fNumberOfTracks;
    *fTracks = *other.fTracks;

    fNumberOfVertexTracks = other.fNumberOfVertexTracks;
    *fVertexTracks = *other.fVertexTracks;

    fNumberOfPSDModules = other.fNumberOfPSDModules;
    fNumberOfFiredPSDModules = other.fNumberOfFiredPSDModules;
    fNumberOfFiredPSDSections = other.fNumberOfFiredPSDSections;

    std::copy(&other.fPsdPosition[0], &other.fPsdPosition[0] + 3, &fPsdPosition[0]);

    fPSDEnergy = other.fPSDEnergy;
    *fPSDModules = *other.fPSDModules;

    fNumberOfTOFHits = other.fNumberOfTOFHits;
    *fTOFHits = *other.fTOFHits;

    // FIXME
    // V0 are skipped because of my (Eugene) laziness

    fNumberOfTriggers = other.fNumberOfTriggers;
    *fTriggers = *other.fTriggers;

    fNumberOfBPDs = other.fNumberOfBPDs;
    *fBPDs = *other.fBPDs;

    fNumberOfWFAs = other.fNumberOfWFAs;
    *fWFAs = *fWFAs;

    fNumberOfMCTracks = other.fNumberOfMCTracks;
    *fMCTracks = *other.fMCTracks;

    *fTrackMatches = *other.fTrackMatches;

    *fPSDPrimaryParticles = *other.fPSDPrimaryParticles;
}


ClassImp(DataTreeEvent);

