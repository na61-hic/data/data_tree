#ifndef DataTreeTrigger_H
#define DataTreeTrigger_H 1

#include <vector>
#include <iostream>
#include "TClonesArray.h"
#include "TObject.h"
#include "TString.h"

#include "DataTreeConstants.h"      

class DataTreeTrigger : public TObject
{
    
public:
  
    DataTreeTrigger(int idx = 0);
    ~DataTreeTrigger();
    
    int GetId(){return fId;}
    
    double GetSignal() const {return fSignal;}
    bool GetIsFired() const {return fIsFired;}
    
    void SetSignal(double value){fSignal = value;}
    void SetIsFired(bool is){fIsFired = is;}
    
private:    
    void SetId(int idx){fId = idx;}
    
    int fId;
    double fSignal;
    bool fIsFired;
    
    ClassDefNV(DataTreeTrigger, 1)
};

#endif
